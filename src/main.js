//variable
const  name = 'Regina';
let url ;
let html = "";

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
//fonctions de sort 
const sortName = function(a,b){
    if(a.name < b.name){
        return -1;
    }
    else return 1;
}
const sortLowPrice = function(a,b){
    if(a.price_small < b.price_small){
        return -1;
    }
    else if(a.price_small==b.price_small){
        return sortHighPrice(a,b);
    }
    else return 1;
}

const sortHighPrice = function(a,b){
    if(a.price_large < b.price_large){
        return -1;
    }
    else return 1;
}
//fonction pour les filtres 

const isBaseTomate= function(a){
    return a.base == "tomate";
}
const doubleI= function(a){
    let reponse =0 ;
    for (let index = 0; index < a.name.length; index++) {
        const element = a.name[index];
        if(element=="i"){
            reponse++;
        }
        
    }
    return reponse == 2;
}

//application des fonctions
data.sort(sortName);
data.sort(sortLowPrice);
const dataFilterBase = data.filter(isBaseTomate);
const dataFilterPrix = data.filter(element=> element.price_small< 6);
const dataFilterI = data.filter(doubleI);

//affichage
data.forEach(element => {
    const{name,base,price_small,price_large,image} = element;
    html +=`<article class="pizzaThumbnail">
    <a href="${image}">
         <img src="${image}"/>
        <section><h4>${name}</h4>
                 <ul>
                    <li> Prix petit format : ${price_small} € </li> 
                    <li> Prix grand format : ${price_large} € </li> 
                 </ul>   
        </section>
    </a>
</article>`;
});
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
