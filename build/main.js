"use strict";

//variable
var name = 'Regina';
var url;
var html = "";
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; //fonctions de sort 

var sortName = function sortName(a, b) {
  if (a.name < b.name) {
    return -1;
  } else return 1;
};

var sortLowPrice = function sortLowPrice(a, b) {
  if (a.price_small < b.price_small) {
    return -1;
  } else if (a.price_small == b.price_small) {
    return sortHighPrice(a, b);
  } else return 1;
};

var sortHighPrice = function sortHighPrice(a, b) {
  if (a.price_large < b.price_large) {
    return -1;
  } else return 1;
}; //fonction pour les filtres 


var isBaseTomate = function isBaseTomate(a) {
  return a.base == "tomate";
};

var doubleI = function doubleI(a) {
  var reponse = 0;

  for (var index = 0; index < a.name.length; index++) {
    var element = a.name[index];

    if (element == "i") {
      reponse++;
    }
  }

  return reponse == 2;
}; //application des fonctions


data.sort(sortName);
data.sort(sortLowPrice);
var dataFilterBase = data.filter(isBaseTomate);
var dataFilterPrix = data.filter(function (element) {
  return element.price_small < 6;
});
var dataFilterI = data.filter(doubleI); //affichage

data.forEach(function (element) {
  var name = element.name,
      base = element.base,
      price_small = element.price_small,
      price_large = element.price_large,
      image = element.image;
  html += "<article class=\"pizzaThumbnail\">\n    <a href=\"".concat(image, "\">\n         <img src=\"").concat(image, "\"/>\n        <section><h4>").concat(name, "</h4>\n                 <ul>\n                    <li> Prix petit format : ").concat(price_small, " \u20AC </li> \n                    <li> Prix grand format : ").concat(price_large, " \u20AC </li> \n                 </ul>   \n        </section>\n    </a>\n</article>");
});
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map